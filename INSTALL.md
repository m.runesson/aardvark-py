# Install Aardvark

This describes how to install Aardvark-py on a Kubernetes/OpenShift cluster.
Prerequisites is that [Tekton](https:/(/tekton.dev/)) is installed on the cluster,
in OpenShift this is the OpenShift Pipeline Operator.

First create a project(namespace in plain Kubernetes):
```shell
oc new project aardvark-py-prod
```

Take the files in [examples directory](examples) and modify them with your
organizations secrets.
* [gitlab-access-token-example.yaml](example-secrets/gitlab-access-token-example.yaml) contains the
token for the user with the rights to read and write to all projects.
* [gitlab-webhook-secret.yaml](example-secrets/gitlab-webhook-secret.yaml) contains the secret
sent together with the webhooks from GitLab to Aardvark to trigger builds etc. You shall
share this with all users that shall be able to set up project to use Aardvark.
* [test-report-config.yaml](example-secrets/test-report-rclone-config.yaml) contains configuration for
[rclone](https://rclone.org/) to upload to S3.
* [twine-secret](example-secrets/twine-secret.yaml) contains user and password for pypi-server.

Once you have modified the files with secrets apply them one by one using
`kubectl apply -f FILENAME`

Deploy the rest of Aardvark:
 `kubectl apply -k kustomize/overlays/prod`


After the installation you can get the webhook-url via:

```shell
kubectl -n aardvark-py-prod get route el-aardvark-py-listener -o jsonpath="https://{.spec.host}"
```

Share this URL and the webhook secret with your users of Aardvark.
These are used when setting up webhooks in GitLab.


The address to the webhook is shown via `kubectl get route`

If you want to have a dedicated test/develop environment to improve Aardvark. Create a project named `aardvark-py-test` and deploy the Kustomize overlay for develop `kustomize/overlays/test`.
